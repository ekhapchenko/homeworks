# Напишите функцию для парсинга номерных знаков автомоблей Украины (стандарты - AА1234BB, 12 123-45AB, a12345BC)
# с помощью регулярных выражений. Функция принимает строку и возвращает None если вся строка не является
# номерным знаком. Если является номерным знаком - возвращает саму строку.

import re


def car_number(number1):

    """The function determines the full match of the number to the pattern.
       The template has three alternatives"""

    tpl1 = r'([A-Z]{2}\d{4}[A-Z]{2})|(\d{2}\ \d{3}\-\d{2}[A-Z]{2})|([a-z]{1}\d{5}[A-Z]{2})'
    res1 = re.fullmatch(tpl1, number1)

    if res1:
        return number1
    else:
        return None


line1 = 'AA1234BB'
print(car_number(line1))

line2 = 'BB3486AA'
print(car_number(line2))

line3 = '12 23A-45AB'
print(car_number(line3))


# Напишите класс, который выбирает из произвольного текста номерные знаки и возвращает их в виде
# пронумерованного списка с помощью регулярных выражений.


class CarNumbers:
    line1 = ''

    def __init__(self, line1):
        self.line1 = line1

    def find_numbers(self):

        """The function returns all matches to the pattern as a numbered list.
           Template has three alternatives"""

        tpl1 = r'([A-Z]{2}\d{4}[A-Z]{2})|(\d{2}\ \d{3}\-\d{2}[A-Z]{2})|([a-z]{1}\d{5}[A-Z]{2})'
        res1 = re.findall(tpl1, self.line1)

        if res1:
            index = 1
            for pattern in res1:
                for number in pattern:
                    if not number:
                        continue
                    print(f'{index}. {number}')
                    index += 1
        else:
            print(None)


my_line = CarNumbers('there are many numbers, for example BC7856BC, also 67 435-12AC, b34865AB ')
my_line.find_numbers()

my_line2 = CarNumbers('there are not any numbers))')
my_line2.find_numbers()


# 3. ** Создайте репозиторий на GitLab или GitHub. Сохраните отдельной веткой
# (пусть будет HW14) дз по регулярным выражениям

# https://gitlab.com/ekhapchenko/homeworks.git





